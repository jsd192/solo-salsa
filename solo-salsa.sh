#!/bin/bash

##Este script es una recopilación de radios salsa online 
echo -e "   Para que funcione este script te comento que\n   el comando mpv debe estar instalado."
if [ "$(which mpv)" = "/usr/bin/mpv" ]; then
	echo -e "\e[1m   Pero NO TE PREOCUPES mpv ESTÁ INSTALDO EN TU DISTRO \e[0m"
##lineas abajo va el while

while :
do
echo "-------------------------------"
echo "SALSA DURA - ESCOGA UNA OPCIÓN "
echo "-- Letra 'q' para detener la reproducción --"
echo "-- Cero 0 Para salir del Script --"
echo ""
echo "1.- Radio Tropicana "
echo "2.- Colombia Crossove  "
echo "3.- Zenith "
echo "4.- Radio Policia Bogotá "
echo "5.- Los 40 Urban de Bogotá "
echo "6.- Fiesta Latina "
echo "7.- Tropical 100 Salsa "
echo "8.- Radio Salsa "
echo "9.- Radio Latina "
echo "10.- Latina Stereo "
echo "11.- Panda Show Radio "
echo "12.- Sabrosita "
echo "13.- Pinguino Stereo "
echo "14.- Colombia Salsa Rosa "
echo "15.- Fiesta FM "
echo "16.- Tropicálida "
echo "17.- Paisa Estereo "
echo "18.- La Máxima "
echo "19.- Fuego 90 "
echo "20.- Salsa Magistral "
echo "0.- Cero, SALIR DEL SCRIPT"
echo ""

echo -n "SU OPCION ELEGIDA ES EL NÚMERO => "

read opcion
case $opcion in
#
1) echo "Radio Tropicana "
mpv https://playerservices.streamtheworld.com/api/livestream-redirect/TROPICANAAAC.aac?dist=onlineradiobox

;;

2) echo "Colombia Crossove "
mpv https://emisoras.colombiacrossover.com/proxy/crossover?mp=/stream

;;

3) echo "Zenith " 
mpv https://onlineradiobox.com/json/cy/zenith/play?platform=web

;;

4) echo "Radio Policia Bogotá " 
mpv https://onlineradiobox.com/json/co/policianacionalbogota/play?platform=web

;;

5) echo "Los 40 Urban de Bogotá " 
mpv https://playerservices.streamtheworld.com/api/livestream-redirect/LOS40URBAN_BOGOTAAAC.aac?dist=onlineradiobox

;;

6) echo "Fiesta Latina " 
mpv https://stream.zeno.fm/e2kzfpr34d0uv

;;

7) echo "Tropical 100 Salsa " 
mpv https://stream.zeno.fm/cjgfujr8yhbvv

;;

8) echo "Radio Salsa " 
mpv https://onlineradiobox.com/json/cu/salsa/play?platform=web

;;

9) echo "Radio Latina " 
mpv https://ice.creacast.com/radio-latina-lu-mp3

;;

10) echo "Latina Stereo " 
mpv https://centova12.instainternet.com/proxy/cohetene?mp=/stream

;;

11) echo "Panda Show Radio " 
mpv https://onlineradiobox.com/json/mx/pandashow/play?platform=web

;;

12) echo "Sabrosita " 
mpv https://playerservices.streamtheworld.com/api/livestream-redirect/XEPHAMAAC.aac?dist=onlineradiobox

;;

13) echo "Pinguino Stereo " 
mpv https://radiolatina.info/9750/stream

;;

14) echo "Colombia Salsa Rosa " 
mpv https://emisoras.colombiacrossover.com/proxy/colsalsarosa?mp=/stream

;;

15) echo "Fiesta FM " 
mpv https://server6.globalhostla.com:9012/stream

;;

16) echo "Tropicálida " 
mpv https://dv9lfri87e89c.cloudfront.net/tropicalida.m3u8

;;

17) echo "Paisa Estereo " 
mpv https://radiolatina.info:10902/

;;

18) echo "La Máxima " 
mpv https://streamming.dobitsoluciones.com:7032/live

;;

19) echo "Fuego 90 " 
mpv https://radio5.domint.net:8110/stream

;;

20) echo "Salsa Magistral " 
mpv https://cloudstream2032.conectarhosting.com/8122/stream

;;

0) echo "LA OPCION CERO ES PARA SALIR DEL SCRIPT, CHAUUUU" ; exit 0

esac
done

##sesto va debajo de done
else
	echo -e "\e[5m   !!!!! Se comprueba que mpv NO esta instalado ¡¡¡¡\e[0m"
	echo "-- Si tu distro esta basado en paquetes .deb como Ubuntu, Linux Mint, Debian"
	echo -e "\e[1m   sudo apt install mpv -y \e[0m"
	echo "-- Si tu distro esta basado en paqutes .rpm como Fedora."
	echo -e "\e[1m   sudo dnf install mpv -y \e[0m"
fi